---
time: 202302141713
title: "bucket"
type: example
---

# BUCKET PROBLEM
--------------------------------------------------------------------------------

continuity equation:
```
  A₍1₎ v₍1₎ = A₍2₎ v₍2₎
```

flux out:
```
  A₍2₎ √{2gh}
```

volumetric flux:
```
  V = A₍2₎v₍2₎t
```

so time taken:
```
  t = V / A₍2₎√{2gh}
```
